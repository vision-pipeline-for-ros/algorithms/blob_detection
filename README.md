![alt text](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/-/raw/master/icons/logo_long.svg)



# Vision Pipeline

[VPFR](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros)

[Wiki](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/wikis/home)

---

# 2D single object detection (Blob)

This is an implementation of a single object blob detection with OpenCV.  
There are several setting options.  

Details on this in the [wiki](https://gitlab.com/vision-pipeline-for-ros/algorithms/blob_detection/-/wikis/home)
