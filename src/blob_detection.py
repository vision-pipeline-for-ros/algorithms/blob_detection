"""
blob_detection
Copyright 2020 Vision Pipeline for ROS
Author: Sebastian Bergner <sebastianbergner5892@gmail.com>
SPDX-License-Identifier: MIT
"""


import cv2
import rospy
from cv_bridge import CvBridge, CvBridgeError
from algorithm_template import AlgorithmTemplate
from sensor_msgs.msg import Image
from vpfr2dobjectdetection.msg import object2D


class BlobDetection(AlgorithmTemplate):
    def get_io_type(self):
        return Image, object2D

    def on_enable(self):
        rospy.loginfo("[algorithm][blob_detection] init")
        self.__bridge = CvBridge()
        self.__ros_to_cv2_encoding = cv2.COLOR_BGR2RGB
        self.__show_image = True
        self.__upper = None
        self.__lower = None
        rospy.loginfo("[algorithm][blob_detection] end")

    def on_disable(self):
        pass

    def main(self, req):
        cv_image = self.__bridge.imgmsg_to_cv2(req)
        cv_image = cv2.cvtColor(cv_image, self.__ros_to_cv2_encoding)
        image = cv_image
        blurred = cv2.GaussianBlur(image, (11, 11), 0)
        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

        mask = cv2.inRange(hsv, self.__lower, self.__upper)
        mask = cv2.erode(mask, None, iterations=1)
        mask = cv2.dilate(mask, None, iterations=1)

        cnts, hirachy = cv2.findContours(
            mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
        )
        cv2.drawContours(image, cnts, -1, (0, 255, 0), 3)
        center = None

        if len(cnts) > 0:
            c = max(cnts, key=cv2.contourArea)
            ((x, y), radius) = cv2.minEnclosingCircle(c)
            M = cv2.moments(c)
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
            if radius > 10:
                cv2.circle(
                    image, (int(x), int(y)), int(radius), (0, 255, 255), 2
                )
                cv2.circle(image, center, 5, (0, 0, 255), -1)
            if self.__show_image:
                self.show(image)
            return object2D(
                int(x), int(y), int(radius) * 2, int(radius) * 2, "blob", None
            )
        if self.__show_image:
            self.show(image)
        return object2D(None, None, None, None, None, None)

    def on_additional_data_change(self, data):
        pass

    # shows the processed image in window
    def show(self, image):
        cv2.imshow("frame", image)
        cv2.waitKey(1)

    def on_config_change(self, item):
        if item is None:
            return
        # upper limit
        upper_R = item.get("upperR")
        if upper_R is not None:
            rospy.loginfo(
                "[algorithm][blob_detection] upperR set to '%s'", upper_R
            )
        else:
            rospy.logwarn(
                "[algorithm][blob_detection] upperR type '%s' not found",
                upper_R,
            )
        upper_G = item.get("upperG")
        if upper_G is not None:
            rospy.loginfo(
                "[algorithm][blob_detection] upperG set to '%s'", upper_G
            )
        else:
            rospy.logwarn(
                "[algorithm][blob_detection] upperG type '%s' not found",
                upper_G,
            )
        upper_B = item.get("upperB")
        if upper_B is not None:
            rospy.loginfo(
                "[algorithm][blob_detection] upperB set to '%s'", upper_B
            )
        else:
            rospy.logwarn(
                "[algorithm][blob_detection] upperB type '%s' not found",
                upper_B,
            )
        if upper_R is None or upper_G is None or upper_B is None:
            rospy.logwarn(
                "[algorithm][blob_detection] setting upper limit failed"
            )
        else:
            self.__upper = (upper_R, upper_G, upper_B)
        # lower limit
        lower_R = item.get("lowerR")
        if lower_R is not None:
            rospy.loginfo(
                "[algorithm][blob_detection] lowerR set to '%s'", lower_R
            )
        else:
            rospy.logwarn(
                "[algorithm][blob_detection] lowerR type '%s' not found",
                lower_R,
            )
        lower_G = item.get("lowerG")
        if lower_G is not None:
            rospy.loginfo(
                "[algorithm][blob_detection] lowerG set to '%s'", lower_G
            )
        else:
            rospy.logwarn(
                "[algorithm][blob_detection] lowerG type '%s' not found",
                lower_G,
            )
        lower_B = item.get("lowerB")
        if lower_B is not None:
            rospy.loginfo(
                "[algorithm][blob_detection] lowerB set to '%s'", lower_B
            )
        else:
            rospy.logwarn(
                "[algorithm][blob_detection] lowerB type '%s' not found",
                lower_B,
            )
        if lower_R is None or lower_G is None or lower_B is None:
            rospy.logwarn(
                "[algorithm][blob_detection] setting lower limit failed"
            )
        else:
            self.__lower = (lower_R, lower_G, lower_B)
